<img src = "https://firebasestorage.googleapis.com/v0/b/world-of-games.appspot.com/o/AngularJs.svg?alt=media&token=e9888f52-5acb-4c7f-8326-44b1222d1438">
# [Angular 1.5+ ES6 & Component API Example App]


> Example Angular 1.5+ (ES6 + Components) codebase that adheres to the [RealWorld](https://github.com/gothinkster/realworld-example-apps) spec and API.

View the **[demo application]()** 

# Getting started

1. Clone repo
2. `npm install`
3. `gulp`

Make sure you have gulp installed globally (`npm install -g gulp`)


# What can we find?
* SignIn/Login Twitter-Facebook
* Classic SignUp/LogIn
* Verify account
* Recover Password
* Profile
* List-Details-GMaps for Shop
* VIP Service


View the **[demo application]()** for see all funcionality


<br />

Thanks to https://thinkster.io

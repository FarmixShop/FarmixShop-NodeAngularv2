import angular from 'angular';

// Create the module where our functionality can attach to
let servicesModule = angular.module('app.services', []);


import UserService from './user.service';
servicesModule.service('User', UserService);

import JwtService from './jwt.service'
servicesModule.service('JWT', JwtService);

import ProfileService from './profile.service';
servicesModule.service('Profile', ProfileService);

import ArticlesService from './articles.service';
servicesModule.service('Articles', ArticlesService);

import CommentsService from './comments.service';
servicesModule.service('Comments', CommentsService);

import TagsService from './tags.service';
servicesModule.service('Tags', TagsService);

import ListService from './list.service';
servicesModule.service('List',ListService);

import DetailService from './details.service';
servicesModule.service('Detail',DetailService);

import ContctService from './contact.service';
servicesModule.service('Contact',ContctService);

import Toastr from './toastr.service';
servicesModule.service('Toastr', Toastr);

import SocialService from './social.service';
servicesModule.service('SocialService', SocialService);


export default servicesModule;

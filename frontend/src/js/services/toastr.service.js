export default class Toastr {
    constructor(toastr) {
      'ngInject';
      this._toastr = toastr;
  
    }
    use(title, description, type){

        switch(type) {
            case 'ok':
            this._toastr.success(title, description);
                break;
            case 'info':
            this._toastr.info(title, description);
                break;
            case 'warning':
            this._toastr.warning(title, description);
                break;
            case 'ko':
            this._toastr.error(title, description);
                break;
            default:
                
        }
    }
}
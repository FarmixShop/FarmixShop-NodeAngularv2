export default class Social {
    constructor (User, $state) {
      'ngInject';
      this._User = User;
      this._$state = $state;
    }
     
    /*
    |--------------------------------------------------------------------------
    | Login with Facebook
    |--------------------------------------------------------------------------
    */

      //Facebook Working ==> Please remove me!! $ctrl.facebook()
        facebook(){
            let that=this;
            FB.login(function(response) {
            if (response.authResponse) {
            FB.api('/me',{fields: 'name,email'}, function(response) {
                let formData={};
                formData.username=response.id;
                formData.email='asdad@asda.asda';
                formData.password="1234";
                console.log(formData);
                that._User.attemptAuth("register", formData).then(
                (res) => {
                    that._$state.go('app.home');
                },
                (err) => {
                    that._User.attemptAuth("login", formData).then(
                    (res) => {
                        that._$state.go('app.home');
                        FB.logout();
                    },
                    (err) => {
                        that.errors = err.data.errors;
                        that.errors.username= err.data.errors.username;
                        that.errors.email= err.data.errors.email;
                        that.errors.message= err.data.errors.message;
                    }
                    )
                }
                )
            });
            } else {
            console.log('User cancelled login or did not fully authorize.');
            }
        });
        }
    /*
    |--------------------------------------------------------------------------
    | Login with Twitter
    |--------------------------------------------------------------------------
    */

    twitter(){
        let that=this;
        OAuth.initialize('B-KFxHnfUyHIdu1LH0f-w0VgG6k', {cache: true});
    
        OAuth.popup('twitter', {cache: true}, function (error, result) { //cache means to execute the callback if the tokens are already present
            if (!error) {
              var promise = result.get(('/1.1/account/verify_credentials.json?include_email=true')).done(function (data) { //https://dev.twitter.com/docs/api/1.1/get/statuses/home_timeline
                console.log(promise.responseJSON);
                let formData={};
                formData.username = promise.responseJSON.name,
                formData.email=promise.responseJSON.email;
                formData.password="1234";
                that._User.attemptAuth("register", formData).then(
                  (res) => {
                    that._$state.go('app.home');
                  },
                  (err) => {
                    that._User.attemptAuth("login", formData).then(
                      (res) => {
                        that._$state.go('app.home');
                        FB.logout();
                      },
                      (err) => {
                        that.errors = err.data.errors;
                        that.errors.username= err.data.errors.username;
                        that.errors.email= err.data.errors.email;
                        that.errors.message= err.data.errors.message;
                      }
                    )
                  }
                )
              });
    
            } else {
                console.log(error);
            }
        });
    
      }
  }
  
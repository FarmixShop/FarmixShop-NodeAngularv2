export default class Commentario {
    constructor(JWT, AppConstants, $http, $q,$state) {
      'ngInject';

      this._AppConstants = AppConstants;
      this._$http = $http;
      this._$state = $state;
    }
    addComment(commentObj){
      return this._$http({
        url: this._AppConstants.api + '/comment',
        method: 'POST',
        data: commentObj,
      }).then((res) => res.data);
    }
    

  }

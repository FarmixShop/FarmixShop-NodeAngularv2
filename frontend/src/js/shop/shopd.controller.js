/**
 * @class ShopDetailCtrl
 * @name ShopDetailCtrl
 * @description It's used to show detail view
 * @param {AngularVariable} AppConstants
 * @param {service} List - It's used to get all products
 * @param {service} Detail - It's used to get detail product
 * @param {AngularVariable} $scope - It's used to do connection between controller and view
 * @param {service} Toastr - Service to show toastr's
 * @param {Angular Variable} $stateParams - Angular variable, it's used to get a url param
 */
class ShopDetailCtrl {
    constructor(Detail,List,User,AppConstants, $scope, $stateParams) {
      'ngInject';
  
      //Global variables
      this.appName = AppConstants.appName;
      this._$scope = $scope;
      $scope.currentUser = User.current;
      this.comment = [];
      
      //Get one product
      Detail.getOne($stateParams.id).then( (res) => {
        this.data = res.data.response;
        var c = res.data.response.comments;
        $scope.comments = c;
      });
      
      //Get all products to populate jcarrousel
      List.getAll().then( (res) => {  
        this.more_info = res.data.response;
        setTimeout(this.slick_populate,100);
      });

      document.getElementById('packselectec').addEventListener('change', ()=>{
        this.calculatemoney();
      });

      //TABS
      //Tab section
      this.tabs=[{
        title: 'Product Description',
        url: 'one.tpl.html'
      }, {
        title: 'Shipping orders',
        url: 'two.tpl.html'
      }];
      
      //Select and click tabs
      $scope.currentTab = 'one.tpl.html';
      $scope.onClickTab = function (tab) {
        $scope.currentTab = tab.url;
      }
      $scope.isActiveTab = function(tabUrl) {
        return tabUrl == $scope.currentTab;
      }
      //COMMENT SECTION
      $scope.submit = function() {
        $scope.comments.push({
          name: $scope.name,
          text: $scope.text
        });
        $scope.name = '';
        return $scope.text = '';
      };
    }

    /*Function to calculate total money*/ 
    calculatemoney(){
      var e = document.getElementById("packselectec");
      var strUser = e.options[e.selectedIndex].value;
      var price = 0;
      this.data.packs.forEach(function(pack) {
       if(pack.pack === eval(strUser)){
        price = pack.price;
       }
      }, this);
      document.getElementById("total-money").innerHTML = strUser*price+' €';
    }

    //Carrousel declaration and init
    slick_populate(){
      $('.your-class').slick({
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 3,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              centerMode: true,
              centerPadding: '40px',
              slidesToShow: 3
            }
          },
          {
            breakpoint: 480,
            settings: {
              arrows: false,
              centerMode: true,
              centerPadding: '40px',
              slidesToShow: 1
            }
          }
        ]
      });
    }
  }
  
  export default ShopDetailCtrl;
  
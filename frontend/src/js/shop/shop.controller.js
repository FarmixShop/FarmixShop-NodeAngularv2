/**
 * @class ShopCtrl
 * @name ShopCtrl
 * @description It's used to show list view
 * @param {AngularVariable} AppConstants
 * @param {service} List - It's used to get all products
 * @param {AngularVariable} $scope - It's used to do connection between controller and view
 * @param {service} Toastr - Service to show toastr's
 * @param {service} ngDialog - Service to show ngDialog modal
 * @param {Angular Variable} $state - To redirect on url
 * @param {service} NgMap - Service to show map and populate markers
 * @param {Angular Variable} $interval - Used to refresh ngMap
 */
class ShopCtrl {
    constructor(List,AppConstants, $scope,$interval,ngDialog,Toastr,NgMap) {
      'ngInject';
      
      //Global and Scope variables
      this.appName = AppConstants.appName;
      this._$scope = $scope;
      this.markersValue = [];
      $scope.search = [];
      $scope.currentPage = 0;
      $scope.pageSize = 1;
      $scope.tagArray = [];
      $scope.showAll = true;
      $scope.search = {};
      //Get all Products
      List.getAll().then( (res) => {
        //Print on $scope all products
        $scope.data = res.data.response;
        //Math operation to calculate number of pages
        $scope.data.forEach(function(product) {
          $scope.numberOfPages=function(){
            return Math.ceil($scope.data.length/$scope.pageSize);
        }
          //Populate markersValue
          this.markersValue.push(product.location);

          //Populate tagArray
          if(product.tags){
            if($scope.tagArray.length !== 0){
              product.tags.forEach(function(tag){
                $scope.tagArray.push({name:tag, on:false});
              });
            }else{
              product.tags.forEach(function(tag){
                $scope.tagArray.push({name:tag, on:false});
              });
            }
          }
        }.bind(this));

        //Interval to refresh markers
        $interval( $scope.GenerateMapMarkers, 2000);
       
      });

      //Check if any action change the original array
      $scope.checkChange = function() {
        for(var t = 0; t < $scope.tagArray.length;t++){
            if($scope.tagArray[t].on){
                $scope.showAll = false;
                return;
            }
        }
        $scope.showAll = true;
      };

      //Used to filter by tags on checkbox
      $scope.myFunc = function(a) {
        if($scope.showAll) { return true; }
        
        var sel = false;
        
        for(var tech=0;tech< $scope.tagArray.length;tech++){
            var t = $scope.tagArray[tech];
            if(t.on && a.tags.indexOf(t.name) > -1){
              return true;   
          }    
        }
        return sel;
    };

    //GENERATING MARKERS ON NGMAP
    $scope.GenerateMapMarkers = function() {
      
      //Set title on any marker
      var markers = [];
      for (var i=0; i<this.markersValue.length ; i++) {
        markers[i] = new google.maps.Marker({
          title: "Hi marker " + i
        })
      }

        //Set value and position on map to all markers
      var numMarkers =this.markersValue.length;
      for (var i = 0; i < numMarkers; i++) {
        var lat =   this.markersValue[i][0];
        var lng = this.markersValue[i][1];
        var latlng = new google.maps.LatLng(lat, lng);
        markers[i].setPosition(latlng);
        markers[i].setMap($scope.map);

        //Detail template
        var contentString = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Platano de Canarias</h1>'+
        '<div id="bodyContent">'+
        '<p><b>Platano Canarias</b>, El plátano es una planta herbácea, que pertenece a la familia musácea, y que ostenta normalmente entre 3 o 4 metros de alto. Su tallo está rodeado por las vainas de las hojas y el fruto que resulta de él, que es una baya que tiene la particularidad de crecer en racimos, es ampliamente apreciado y consumido en el mundo como alimento. Popularmente se lo conoce como banano o banana.'+
        '</div>'+
        '</div>';

        //Show info on window when click
        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

        markers[i].addListener('click', function() {
          infowindow.open($scope.map, markers[i]);
        });
      }      
    }.bind(this);

    //Init the ngMap
    $scope.initMap = function(mapId) {
      $scope.map = NgMap.initMap(mapId);
    }
  }
    
  }

  export default ShopCtrl;
  
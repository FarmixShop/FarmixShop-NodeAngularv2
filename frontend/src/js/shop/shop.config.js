function ShopConfig($stateProvider) {
    'ngInject';
    $stateProvider.state('app.shop', {
        url: '/list',
        controller: 'ShopCtrl',
        controllerAs: '$shopCtrl',
        templateUrl: 'shop/shopList.html',
        title: 'Harvest'
    })
    .state('app.shopdetail', {
        url: '/detail/:id',
        controller: 'ShopDetailCtrl',
        controllerAs: '$shopDetailCtrl',
        templateUrl: 'shop/shopDetail.html',
        title: 'Detail'
    });
};
  
  export default ShopConfig;
  
/**
 * @class HomeCtrl
 * @name HomeCtrl
 * @description It's used to populate home.html
 * @param {service} List - It's used to get all products
 * @param {AngularVariable} AppConstants
 * @param {AngularVariable} $scope - It's used to do connection between controller and view
 * @param {service} Toastr - Service to show toastr's
 * @param {service} Messages - Service to recive and send messages to PubNub
 * @param {service} ngDialog - Service to show ngDialog modal
 */
class HomeCtrl {
  constructor(List,AppConstants, $scope,Toastr,Messages,ngDialog) {
    'ngInject';

    //Global variables
    this.appName = AppConstants.appName;
    this._$scope = $scope;
    $scope.messages = [];

    //Check localStorage toastr
   if(localStorage.getItem('toastr')){
     Toastr.use('Payment complete', 'Your payment has been successful', 'ok')
     localStorage.clear();
   }
    // Get list of all tags
    List.getAll().then( (res) => {
      $scope.data = res.data.response;
    });

    //Close all ngDialog
    ngDialog.close();
    
    //Open chat ngDialog
    $scope.chat = ()=>{
      ngDialog.open({ template: 'chat', className: 'ngdialog-theme-default', controller: 'ChatCtrl', scope: $scope });
    }
    
    // Receive Messages 
    Messages.receive(function(message){
        $scope.messages.push(message);
    });
    // Send Messages 
    $scope.send = function() {
        Messages.send({ data : $scope.textbox });
    };
  }
}

export default HomeCtrl;

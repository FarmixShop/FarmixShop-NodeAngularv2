function AuthConfig($stateProvider, $httpProvider) {
  'ngInject';

  $stateProvider

  .state('app.login', {
    url: '/login',
    controller: 'AuthCtrl as $ctrl',
    templateUrl: 'auth/auth.html',
    title: 'Sign in',
    resolve: {
      auth: function(User) {
        return User.ensureAuthIs(false);
      }
    }
  })
  .state('app.recover', {
    url: '/recover',
    controller: 'AuthCtrl as $ctrl',
    templateUrl: 'auth/recoverPassword.html',
    title: 'Recover Password',
    resolve: {
      auth: function(User) {
        return User.ensureAuthIs(false);
      }
    }
  })
  .state('app.newpassword', {
    url: '/newpassword/:resetpassword',
    controller: 'AuthCtrl as $ctrl',
    templateUrl: 'auth/newpassword.html',
    title: 'New Password',
    resolve: {
      auth: function(User, $state, $stateParams) {
        return User.ensureAuthIs(false);
      }
    }
  })

  .state('app.register', {
    url: '/register',
    controller: 'AuthCtrl as $ctrl',
    templateUrl: 'auth/auth.html',
    title: 'Sign up',
    resolve: {
      auth: function(User) {
        return User.ensureAuthIs(false);
      }
    }
  })
  .state('app.active', {
    url: '/active/:token',
    controller: 'AuthCtrl as $ctrl',
    resolve: {
      auth: function(User, $state, $stateParams) {

        return User.get($stateParams.token).then(
            (User)=> $state.go('app.home'),
            (err) => $state.go('app.home')
          );

      }
    }
  });

};

export default AuthConfig;

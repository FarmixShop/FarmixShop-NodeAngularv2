/**
 * @class AuthCtrl
 * @name AuthCtrl
 * @description It's used to populate home.html
 * @param {service} User - It's used to get a user and activate her account
 * @param {AngularVariable} AppConstants
 * @param {AngularVariable} $scope - It's used to do connection between controller and view
 * @param {service} Toastr - Service to show toastr's
 * @param {service} ngDialog - Service to show ngDialog modal
 * @param {service} SocialService - Service to make auth with facebook and twitter
 * @param {Angular Variable} $state - To redirect on url
 */
class AuthCtrl {
  constructor(User, $state,ngDialog,$scope,SocialService,Toastr) {
    'ngInject';

    //Global variables
    this._User = User;
    this._$state = $state;
    this._toastr = Toastr;
    this.title = $state.current.title;
    this.authType = $state.current.name.replace('app.', '');
    $scope.title = this.title;

    //Close all ngDialog
    ngDialog.close();

    //Open a new ngDialog with auth template
    this.test = ngDialog.open({ template: 'templateId', className: 'ngdialog-theme-default', controller: 'MethodsCtrl', scope: $scope });

    //LOGIN FUNCTIONS
    //Facebook function
    $scope.facebook = ()=>{
      SocialService.facebook();
    }
    //Twitter function
    $scope.twitter = ()=>{
      SocialService.twitter();
    }

  }

  //Function to check if user exist on MongoDB
  submitForm() {
    this.formData.newPassword = this._$state.params.resetpassword;
     this.isSubmitting = true;
     //console.log("Eoooo Crack" + this.formData.password)
     this._User.attemptAuth(this.authType, this.formData).then(
       (res) => {

         if(res.config.url=="http://localhost:3000/api/users"){
           this._User.logout();
           this._$state.go('app.home');
           //this._toastr.use('Email Send','Please, check your email to activate your account', 'info');
         }else if (res.config.url=="http://localhost:3000/api/users/recover") {
           this._User.logout();
           this._$state.go('app.home');

         }else if  (res.config.url=="http://localhost:3000/api/users/newpassword") {
           this._User.logout();
           this._$state.go('app.home');

         }else{
           this._$state.go('app.home');
         }
       },
       (err) => {
         this.isSubmitting = false;
           this.errors = err.data.errors;
       }
     )

   }

}

export default AuthCtrl;

import angular from 'angular';

// Create the module where our functionality can attach to
let paymentsModule = angular.module('app.payments', []);

// Include our UI-Router config settings
import PaymentsConfig from './payments.config';
paymentsModule.config(PaymentsConfig);


// Controllers
import PaymentsCtrl from './payments.controller';
paymentsModule.controller('PaymentsCtrl', PaymentsCtrl);


export default paymentsModule;

/**
 * @class PaymentsCtrl
 * @name PaymentsCtrl
 * @description It's used to show payments view
 * @param {AngularVariable} AppConstants
 * @param {AngularVariable} $scope - It's used to do connection between controller and view
 * @param {service} Toastr - Service to show toastr's
 */
class PaymentsCtrl {
    constructor(AppConstants, $scope,Toastr) {
      'ngInject';

      //Global variables and stripe key
      Stripe.setPublishableKey('pk_test_FKLp6F7WNkvHBlM0xcQUaTQR');
      this.appName = AppConstants.appName;
      this._$scope = $scope;

      //Set on LocalStorage
      let iconTick =  document.getElementById('payment-pattern').addEventListener('click',function(){
        localStorage.setItem('toastr','ok');
      });
    }
  
  
  
  }
  
  export default PaymentsCtrl;
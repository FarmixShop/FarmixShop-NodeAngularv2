function PaymentsConfig($stateProvider) {
    'ngInject';
  
    $stateProvider
    .state('app.payments', {
      url: '/payments',
      controller: 'PaymentsCtrl',
      controllerAs: '$ctrl',
      templateUrl: 'payments/payments.html',
      title: 'Payments'
    });
  
  };
  
  export default PaymentsConfig;
  
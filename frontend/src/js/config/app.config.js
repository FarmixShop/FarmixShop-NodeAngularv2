import authInterceptor from './auth.interceptor'

function AppConfig($httpProvider, $stateProvider, $locationProvider, $urlRouterProvider,$translateProvider) {
  'ngInject';

  var translationsEN = {
    HOME:'HOME',
    SHOP:'SHOP',
    CONTACT:'CONTACT',
    WELCOME:'Welcome to FarmixShop',
    BEST:'Best Products',
    UPGRADE:'Do you wand upgrade to vip account?'
  };
  
  var translationsES= {
    HOME:'INICIO',
    SHOP:'TIENDA',
    CONTACT:'CONTACTO',
    WELCOME:'FarmixShop',
    BEST:'Mejores Productos',
    UPGRADE:'Quieres mejorar la cuenta a VIP?'
  };

  $httpProvider.interceptors.push(authInterceptor);
  $translateProvider.translations('en', translationsEN);
  $translateProvider.translations('es', translationsES);
  $translateProvider.fallbackLanguage('en');
  $translateProvider.preferredLanguage('en');
  /*
    If you don't want hashbang routing, uncomment this line.
    Our tutorial will be using hashbang routing though :)
  */
  // $locationProvider.html5Mode(true);
  
  $stateProvider
  .state('app', {
    abstract: true,
    templateUrl: 'layout/app-view.html',
    resolve: {
      auth: function(User) {
        return User.verifyAuth();
      }
    }
  });

  $urlRouterProvider.otherwise('/');

}

export default AppConfig;

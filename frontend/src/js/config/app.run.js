function AppRun(AppConstants, $rootScope,$templateCache) {
  'ngInject';

  $templateCache.put('home1.html', ' <div class="image-home"> <img src="img/newback.png"/> <div class="test"> <h1 class="typing">Welcome to FarmixShop</h1> </div></div><div class="search-responsive"> <div class="search-content-responsive"> <input type="search" name="" value="" placeholder="Look at our harvest..." class="search-input"> <button type="button" class="btn-search" name="button"><i class="fa fa-search" aria-hidden="true"></i></button> </div></div>');
  $templateCache.put('home2.html',' <div class="vip-section"> <h1 translate="UPGRADE"></h1> <a type="button" ui-sref-active="active" class="btn-vip" ui-sref="app.payments">Click here!</a> </div>')
  $templateCache.put('footerTPL.html','<div class="sticky" ng-click="chat()">Live support <i class="fa fa-circle Blink"></i></div><footer class="footer"> <div class=""> <h1>FarmixShop Info</h1> <p>IES Lestació</p><p>Lo que vullges</p></div><div class="payment-methods"> <h4>Payment Methods</h4> <div> <i class="fa fa-cc-stripe" aria-hidden="true"></i> <i class="fa fa-cc-paypal" aria-hidden="true"></i> <i class="fa fa-cc-mastercard" aria-hidden="true"></i> </div><div > <i class="fa fa-cc-amex" aria-hidden="true"></i> <i class="fa fa-cc-visa" aria-hidden="true"></i> </div></div></footer>')
  // change page title based on state
  $rootScope.$on('$stateChangeSuccess', (event, toState) => {
    $rootScope.setPageTitle(toState.title);
  });

  // Helper method for setting the page's title
  $rootScope.setPageTitle = (title) => {
    $rootScope.pageTitle = '';
    if (title) {
      $rootScope.pageTitle += title;
      $rootScope.pageTitle += ' \u2014 ';
    }
    $rootScope.pageTitle += AppConstants.appName;
  };

}

export default AppRun;

/**
 * @class ContactCtrl
 * @name ContactCtrl
 * @description It's used to populate home.html
 * @param {service} Toastr - It's to show toast dialog
 * @param {AngularVariable} AppConstants
 * @param {AngularVariable} $scope - It's used to do connection between controller and view
 * @param {service} Toastr - Service to show toastr's
 * @param {Angular Variable} $state - To redirect on url
 */
class ContactCtrl {
  constructor($scope, Contact,$state,Toastr) {
    'ngInject';

    //Global variables
    $scope.contact = {
    inputName: "",
    inputEmail: "",
    inputSubject: "",
    inputMessage: ""
  };

    /*SEND EMAIL FUNCTION*/
    $scope.SendContact = function () {
      var data = {
      "type":"contact",
      "inputName": $scope.user.name,
      "inputEmail": $scope.user.email,
      "inputSubject": $scope.user.subject,
      "inputMessage": $scope.user.msg,
      "location":window.location.origin,
      "token":'userForm'
    };

    var userForm = JSON.stringify(data);
    
    //Function to go server to send email by SendGrid
    Contact.sendEmail(userForm).then(function(res,err){
        if(res){
            $state.go("app.home");
            Toastr.use('Send email', 'Email success send', 'ok')
          }else{
            $state.go("app.home");
            Toastr.use('Error email', 'Error to send email', 'ko')
          }
        });
      };

  }

}

export default ContactCtrl;

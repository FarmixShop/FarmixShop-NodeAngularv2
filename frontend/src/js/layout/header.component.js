/**
 * @class AppHeaderCtrl
 * @name AppHeaderCtrl
 * @description It's used to show header
 * @param {AngularVariable} AppConstants
 * @param {AngularVariable} $scope - It's used to do connection between controller and view
 * @param {service} User - Service to get users on MongoDB
 */
class AppHeaderCtrl {
  constructor(AppConstants, User, $scope) {
    'ngInject';

    //Global variables
    this.appName = AppConstants.appName;
    this.currentUser = User.current;
    this.logout = User.logout.bind(User);
    
    //Show vip when user are auth and her account is vip
    if(User.current != null){
      this.vip = User.current.vip;
    }    

    //Check if User.current change
    $scope.$watch('User.current', (newUser) => {
      this.currentUser = newUser;
    })
    

  }
}

let AppHeader = {
  controller: AppHeaderCtrl,
  templateUrl: 'layout/header.html'
};

export default AppHeader;

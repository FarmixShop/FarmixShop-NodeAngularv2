/**
 * @class AppFooterCtrl
 * @name AppFooterCtrl
 * @description It's used to populate home.html
 * @param {AngularVariable} AppConstants
 * @param {AngularVariable} $scope - It's used to do connection between controller and view
 * @param {service} ngDialog - Service to show ngDialog modal
 * @param {AngularVariable} $translate - It's used to change lang
 */
class AppFooterCtrl {
  constructor(AppConstants,ngDialog,$scope,$translate) {
    'ngInject';
    //Global variables
    this.appName = AppConstants.appName;

    //Close all ngDialog
    ngDialog.close();

    //Open ngDialog chat view
    $scope.chat = ()=>{
      ngDialog.open({ template: 'chat', className: 'ngdialog-theme-default', controller: 'ChatCtrl', scope: $scope });
    }

    //Change lang
    $scope.changeLanguage = function (langKey) {
      $translate.use(langKey);
    };
  }
}

let AppFooter = {
  controller: AppFooterCtrl,
  templateUrl: 'layout/footer.html'
};

export default AppFooter;

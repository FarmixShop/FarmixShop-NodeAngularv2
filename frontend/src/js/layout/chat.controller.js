/**
 * @class ChatCtrl
 * @name ChatCtrl
 * @description It's used to populate chat modal
 * @param {AngularVariable} AppConstants
 * @param {AngularVariable} $scope - It's used to do connection between controller and view
 * @param {service} Messages - Service to recive and send messages to PubNub
 */
class ChatCtrl {
    constructor(AppConstants,$scope,Messages) {
      'ngInject';

      //Global variables
      this.appName = AppConstants.appName;
      this._$scope = $scope;
      $scope.messages = [];

      // Receive Messages 
      Messages.receive(function(message){
          $scope.messages.push(message);
      });
      // Send Messages 
      $scope.send = function() {
          Messages.send({ data : $scope.textbox });
      };
    }
  
  
  
  }
  
  export default ChatCtrl;
  
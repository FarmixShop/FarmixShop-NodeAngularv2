var mongoose = require('mongoose');

var TesteoSchema = new mongoose.Schema({
    item: String,
    a:Number
}, {timestamps: true});

// Requires population of author
TesteoSchema.methods.toJSONFor = function(){
  return {
    id: this._id,
    item: this.item,
    a: this.a
  };
};

mongoose.model('Testeo', TesteoSchema);

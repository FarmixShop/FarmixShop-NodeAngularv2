var mongoose = require('mongoose');

var FarmixDataSchema = new mongoose.Schema({
    name_user: String,
    products:Array
}, {timestamps: true});

// Requires population of author
FarmixDataSchema.methods.toJSONFor = function(){
  return {
    id: this._id,
    name_user: this.name_user,
    products: this.products
  };
};

mongoose.model('FarmixData', FarmixDataSchema);

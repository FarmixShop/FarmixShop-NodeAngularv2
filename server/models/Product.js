var mongoose = require('mongoose');

var ProductSchema = new mongoose.Schema({
    _id:Number,
    name: String,
    type:String,
    quantity:Number,
    price:Number,
    offer:Boolean,
    offerPrice:Number,
    descripcion:String,
    comments:JSON,

}, {timestamps: true});

ProductSchema.methods.toJSONFor = function(){
  return {
    id: this._id,
    name: this.name,
    quantity: this.quantity,
    price: this.price,
    offer: this.offer,
    offerPrice: this.offerPrice,
    descripcion: this.descripcion,
    comments: this.comments,
  };
};

mongoose.model('Product', ProductSchema);

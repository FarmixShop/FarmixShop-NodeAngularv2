var mongoose = require('mongoose');

var CommentsSchema = new mongoose.Schema({
  comment: String,
  user: String,
}, {timestamps: true});

// Requires population of author
CommentsSchema.methods.toJSONFor = function(user){
  return {
    id: this._id,
    comment: this.comment,
    createdAt: this.createdAt,
    user: this.user
  };
};

mongoose.model('Comments', CommentsSchema);

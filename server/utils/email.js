var nodemailer = require('nodemailer');
var sgTransport = require('nodemailer-sendgrid-transport');
var sgSendGrid = require ('../data/credentials.json');
var sg = sgSendGrid.SENDGRID_API_KEY;


exports.sendEmail = function(req, res) {

  var emailTo = '';
  var emailFrom = '';
  var problem = '';
  var body = '';

  switch (req.body.type) {
    case 'user':
      break;
    case 'admin':
      break;
    case 'contact':

      emailTo = req.body.inputEmail;
      emailFrom = "shifirit@gmail.com";
      ruta = '<a href="'+ req.body.location +'"'+'>aqu&iacute;</a>';
      body = '<body>' +
        '<div id="contact-email">' +
        '<div> <h1>Contact with FarmixShop</h1> <h4>Subject: ' + req.body.inputSubject +
        '</h4></div>' +
        '<section>' +
        'Name:<p>' + req.body.inputName + '</p>' +
        'Email: <p>' + req.body.inputEmail + '</p>' +
        'Message:<p>' + req.body.inputMessage + '</p></section>' + '<br><br>' + 'Gracis por avisarnos, pronto nos pondremos en contacto contigo' + '<br><br>' +
        'Para visitar nuestra web, pulsa ' + ruta +
        '</div>' +
        ' </body>';
      //problem = 'Tu peticion a FarmixShop ha sido enviada correctamente';
      //ruta = '<a href="'+ req.body.location +'"'+'>aqu&iacute;</a>';
    //  body ='Hola ' + req.body.inputName + '<br>' + 'Tu problema de tipo ' + '<strong>' + req.body.inputSubject + '</strong>' + ' con descripcion: ' + '<br>' +
      //req.body.inputMessage + '<br><br>' + 'Gracis por avisarnos, pronto nos pondremos en contacto contigo' + '<br><br>' +
      //'Para visitar nuestra web, pulsa ' + ruta;
    //  console.log(problem);
    //  console.log(ruta);
    //  console.log(body);
      break;
    case 'modify':
      break;
    case 'signup':
      break;


  }


  var template =
    '<html>' +
    '<head>' +
    '<meta charset="utf-8" />' +
    '<style>' +
    '* {' +
    'box-sizing: border-box;' +
    '-webkit-box-sizing: border-box;' +
    '-moz-box-sizing: border-box;' +
    '-webkit-font-smoothing: antialiased;' +
    '-moz-font-smoothing: antialiased;' +
    '-o-font-smoothing: antialiased;' +
    'font-smoothing: antialiased;' +
    'text-rendering: optimizeLegibility;}' +
    ' body { color: #C0C0C0; font-family: Arial, san-serif;}' +
    ' h1 { margin: 10px 0 0 0;}' +
    ' h4 { margin: 0 0 20px 0;}' +
    ' #contact-email {' +
    'background-color: #6CFA21;' +
    'padding: 10px 20px 30px 20px;' +
    ' max-width: 100%;' +
    ' float: left;' +
    'left: 50%;' +
    'position: absolute;' +
    'margin-top: 30px;' +
    ' margin-left: -260px;' +
    ' border-radius: 7px;' +
    '-webkit-border-radius: 7px;' +
    '-moz-border-radius: 7px;}' +
    ' #contact-email p { font-size: 15px; margin-bottom: 10px;' +
    'font-family: Arial, san-serif; }' +
    ' #contact-email p {' +
    'width: 100%;' +
    'background: #fff;' +
    'border: 0;' +
    '-moz-border-radius: 4px;' +
    '-webkit-border-radius: 4px;' +
    ' border-radius: 4px;' +
    ' margin-bottom: 25px;' +
    ' padding: 10px; }' +
    '@media only screen and (max-width: 580px) {' +
    '#contact-form {' +
    ' left: 3%;' +
    ' margin-right: 3%;' +
    ' width: 88%;' +
    ' margin-left: 0;' +
    ' padding-left: 3%;' +
    ' padding-right: 3%; } }' +
    '</style>' +
    '</head>' + body + '</html>';

  var email = {
    from: emailFrom,
    to: emailTo,
    subject: req.body.inputSubject,
    text: req.body.inputMessage,
    html: template
  };
  console.log(email);

  //Input APIKEY Sendgrid
  var options = {
    auth: {
      api_key: sg
    }
  };
  console.log(options);

  var mailer = nodemailer.createTransport(sgTransport(options));

  mailer.sendMail(email, function(error, info) {
    if (error) {
      res.status('401').json({
        err: info
      });
    } else {
      res.status('200').json({
        success: true
      });
    }
  });

};

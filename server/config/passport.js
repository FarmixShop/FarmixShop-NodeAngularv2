var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var User = mongoose.model('User');
var FacebookStrategy = require('passport-facebook').Strategy;
var TwitterStrategy = require('passport-twitter').Strategy;

passport.use(new LocalStrategy({
  usernameField: 'user[email]',
  passwordField: 'user[password]'
}, function(email, password, done) {
  User.findOne({email: email}).then(function(user){
    if(!user || !user.validPassword(password)){
      return done(null, false, {errors: {'email or password': 'is invalid'}});
    }

    return done(null, user);
  }).catch(done);
}));

passport.use(new FacebookStrategy({
  clientID: '889786851146641',
  clientSecret: 'dd685e22539d2e1a50c5f90c0d30d5b3',
  callbackURL: "http://localhost:3000/api/facebook/callback",
  profileFields:['id','displayName','email']
},
function(accessToken, refreshToken, profile, done) {
  console.log(profile)
 //check user table for anyone with a facebook ID of profile.id
 User.findOne({
  'facebook.id': profile.id 
}, function(err, user) {
  if (err) {
      return done(err);
  }
  //No user was found... so create a new user with values from Facebook (all the profile. stuff)
  console.log(profile.displayName)
  if (!user) {
      user = new User({
        id: profile.id,
          name: profile.displayName,
          email: 'asda@æsdad.asd',
          username: 'as',
          provider: 'facebook',
          //now in the future searching on User.findOne({'facebook.id': profile.id } will match because of this next line
          facebook: profile._json
      });
      user.save(function(err) {
          if (err) console.log(err);
          return done(err, user);
      });
  } else {
      //found user. Return
      return done(err, user);
  }
});
}
));


var router = require('express').Router();
var mongoose = require('mongoose');
var Product = mongoose.model('Product');

// return a list of tags
router.get('/', function(req, res, next) {
  Product.find().then(function(response){
    return res.json({response});
  }).catch(next);
});

module.exports = router;

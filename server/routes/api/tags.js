var router = require('express').Router();
var mongoose = require('mongoose');
var Testeo = mongoose.model('Testeo');

router.get('/', function(req, res, next) {
  Testeo.find().then(function(response){
    return res.json({response});
  }).catch(next);
});

module.exports = router;

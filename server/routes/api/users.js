var mongoose = require('mongoose');
var router = require('express').Router();
var passport = require('passport');
var User = mongoose.model('User');
var auth = require('../auth');
var stripe = require('stripe')('sk_test_47nzBezdRsaqGatqj6gREXPQ');
var email= require("../../utils/email");
var sgSendGrid = require ('../../data/credentials.json');
var fbkey = sgSendGrid.FACEBOOK_SECRET;
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: sgSendGrid.TOKEN,
    pass: sgSendGrid.PASS
  }
});

router.get('/user', auth.required, function(req, res, next){
  User.findById(req.payload.id).then(function(user){
    if(!user){ return res.sendStatus(401); }

    return res.json({user: user.toAuthJSON()});
  }).catch(next);
});

router.put('/user', auth.required, function(req, res, next){
  User.findById(req.payload.id).then(function(user){
    if(!user){ return res.sendStatus(401); }

    // only update fields that were actually passed...
    if(typeof req.body.user.username !== 'undefined'){
      user.username = req.body.user.username;
    }
    if(typeof req.body.user.email !== 'undefined'){
      user.email = req.body.user.email;
    }
    if(typeof req.body.user.bio !== 'undefined'){
      user.bio = req.body.user.bio;
    }
    if(typeof req.body.user.image !== 'undefined'){
      user.image = req.body.user.image;
    }
    if(typeof req.body.user.password !== 'undefined'){
      user.setPassword(req.body.user.password);
    }

    return user.save().then(function(){
      return res.json({user: user.toAuthJSON()});
    });
  }).catch(next);
});

router.post('/users/login', function(req, res, next){
  if(!req.body.user.email){
    return res.status(422).json({errors: {email: "can't be blank"}});
  }

  if(!req.body.user.password){
    return res.status(422).json({errors: {password: "can't be blank"}});
  }

  passport.authenticate('local', {session: false}, function(err, user, info){
    if(err){ return next(err); }

    if(user){
      if(user.activo==1){
        user.token = user.generateJWT();
        return res.json({user: user.toAuthJSON()});

      }else{
        return res.status(422).json({errors: {activate: "Revisa el correo"}});
      }

    } else {
      return res.status(422).json(info);
    }
  })(req, res, next);
});
router.post('/users/recover', function(req, res, next){

  User.findOne({email:req.body.user.email}).then(function(user){
    let tknpasswd= user.tknPasswd();

    if(!user){
      return res.sendStatus(401);
    }


    user.resetpassword = tknpasswd;
    user.save().then(
    transporter.sendMail(mailOptions = {
        from:'FarmixShop@gmail.com',
        to: user.email,
        subject: 'Restore Password',
        html: '<h1>Reset passwd in FarmixShop</h1><div>Click: <br> http://localhost:4000/#!/newpassword/'+user.resetpassword+'</div>'
      }
      , function(error, info){
        if (error) {
          console.log('Hola' + error);
        } else {
          console.log('Hola2: ' + info.response);
        }
      })
  );

    return res.json({user: user.toAuthJSON()});
  }).catch(next);
});

router.post('/users/newpassword', function(req, res, next){
  User.findOne({resetpassword:req.body.user.newPassword}).then(function(user){
    if(!user){
      return res.sendStatus(401);
    }

    if(typeof req.body.user.password !== 'undefined'){
      user.setPassword(req.body.user.password);
      user.resetpassword = user.tknPasswd();
    }
    user.save();

    return res.json({user: user.toAuthJSON()});
  }).catch(next);
});

router.post('/users', function(req, res, next){
  var user = new User();

  user.username = req.body.user.username;
  user.email = req.body.user.email;
  user.setPassword(req.body.user.password);
  user.activo = 0;

  user.save().then(function(){
    return res.json({user: user.toAuthJSON()});
  }).catch(next);
  // Añadir la template al email.js
  transporter.sendMail(mailOptions = {
      from: 'FarmixShop@gmail.com',
      to: user.email,
      subject: 'Activation account in FamixShop',
      html: '<h1>Welcome to FamixShop</h1><div>Click on the link to activate the account: <br>http://localhost:4000/#!/active/'+ user.salt
      +'</div>'
    }
    , function(error, info){
      if (error) {
        console.log("Hola" + error);
      } else {
        console.log("Hola enviando: " + info.response);
      }
    });

});

router.post('/stripe',function(req, res, next){

  var token = req.body.stripeToken;
  var chargeAmount = req.body.chargeAmount;

  var charge = stripe.charges.create({
    amount: '0100',
    currency: "eur",
    source: token,
  }, function(err,charge){
    User.update({email:req.body.stripeEmail},{$set:{vip:'ok'}},function(err,numberAffected){
    });
    res.redirect('http://localhost:4000/#!/payaments');
  });
});
router.get('/users/active/:token', function(req, res, next) {

  User.update({salt:req.params.token},{$set:{"activo":1}}).then(function(user){

    User.findOne({salt:req.params.token}).then(function(user){

      if(!user){
        console.log("Hola");
        return res.sendStatus(401);
      }

      return res.json({user: user.toAuthJSON()});
    });

  }).catch(next);

});
module.exports = router;
